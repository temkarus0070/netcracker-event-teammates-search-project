package org.netcracker.eventteammatessearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventTeammatesSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventTeammatesSearchApplication.class, args);
    }

}
